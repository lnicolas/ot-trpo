# Optimal Transport Trust Region Policy Optimization

<p align="center">
    <a href="https://youtu.be/TODO">
        <img src="./resources/TODO" alt="Optimal Transport Trust Region Policy Optimization"/>
    </a>
</p>

This repository contains the code of the paper [Trust Region Policy Optimization with Optimal Transport Discrepancies: Duality and Algorithm for Continuous Actions](TODO), Neural Information Processing Systems (NeurIPS) 2022, New Orleans (LA), USA.

If you use this code in an academic context, please cite:

```bibtex
@inproceedings{ottrpo2022,
  title={Trust Region Policy Optimization with Optimal Transport Discrepancies: Duality and Algorithm for Continuous Actions},
  author={Terpin, Antonio and Lanzetti, Nicolas and Batuhan, Yardim and Dörfler, Florian and Ramponi, Giorgia},
  booktitle={NeurIPS},
  year={2022},
  organization={PMLR}
}
```

## Abstract
Policy Optimization (PO) algorithms have been proven particularly suited to handle the high-dimensionality of real-world continuous control tasks. In this context, Trust Region Policy Optimization methods represent a popular approach to stabilize the policy updates. These usually rely on the Kullback-Leibler (KL) divergence to limit the change in the policy. The Wasserstein distance represents a natural alternative, in place of the KL divergence, to define trust regions or to regularize the objective function. However, state-of-the-art works either resort to its approximations or do not provide an algorithm for continuous state-action spaces, reducing the applicability of the method.
In this paper, we explore optimal transport discrepancies (which include the Wasserstein distance) to define trust regions, and we propose a novel algorithm -- Optimal Transport Trust Region Policy Optimization (OT-TRPO) -- for continuous state-action spaces. We circumvent the infinite-dimensional optimization problem for PO by providing a one-dimensional dual reformulation for which strong duality holds.
We then analytically derive the optimal policy update given the solution of the dual problem. This way, we bypass the computation of optimal transport costs and of optimal transport maps, which we implicitly characterize by solving the dual formulation.
Finally, we provide an experimental evaluation of our approach across various control tasks. Our results show that optimal transport discrepancies can offer an advantage over state-of-the-art approaches.

<p align="center">
  <img alt="HalfCheetah" src="./resources/HalfCheetah.gif" width="30%">
&nbsp; &nbsp;
  <img alt="Hopper" src="./resources/Hopper.gif" width="30%">
&nbsp; &nbsp;
<img alt="Swimmer" src="./resources/Swimmer.gif" width="30%">
</p>

## Getting started

Herein we provide code for both discrete and continuous spaces. 

- Our code in continuous spaces interfaces with Stable Baselines. Please refer to the [documentation](https://stable-baselines.readthedocs.io/en/master/).
- To reproduce the results in discrete spaces, install the `requirements.txt` and then launch `run.py`. To switch between `Taxi-v3` and `CliffWalking-v0`, change line 12:

```
(env_name, simEnv) = ENVIRONMENTS.CLIFF # CliffWalking-v0
(env_name, simEnv) = ENVIRONMENTS.TAXI # Taxi-v3
```

## Results

Below we summarize the comparisons with state-of-the-art baseline algorithms. The shaded area represents the mean ± the standard deviation of the cumulative rewards across 10 independent runs. Every policy evaluation in each run is averaged over 10 sampled trajectories.

<figure class="image">
  <img src="./resources/LearningCurves.png" alt="Learning curves in different environments">
</figure>
