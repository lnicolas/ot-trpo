# Distance used
def binary_distance(a1: int, a2: int) -> float:
    return float(a1 != a2)

# taxi distance
def taxi_distance(a1: int, a2: int) -> float:
    if a1 == a2:
        return 0.0
    elif a2 <= 3:
        return 1.0
    elif a2 >= 4:
        return 1.5